define mpd::output (
  Enum['shout', 'null', 'fifo', 'pipe', 'alsa', 'roar', 'ao', 'oss',
  'openal', 'pulse', 'jack', 'httpd', 'recorder'] $type,
  Optional[Hash] $parameters = {},
) {
  require ::mpd::server

  $content_base = @("EOF")
    audio_output {
      type "${type}"
      name "${name}"
    | EOF

  $content_aux = $parameters.map |$k, $v| { "${k} \"${v}\"\n" }.join()

  concat::fragment { "mpd audio ${name}":
    target  => '/etc/mpd.conf',
    content => "${content_base}${content_aux}}\n",
  }
}
