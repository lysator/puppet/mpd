type Permission = Enum['read', 'add', 'control', 'admin']

type Password = Struct[{ name => String, permissions => Array[Permission]}]

type Metadata = Variant[Enum['none'],
                  Array[Enum['artist', 'album', 'title',
                              'track', 'name', 'genre',
                              'date', 'composer', 'performer',
                              'comment', 'disc']]]

# See mpd(5)
# TODO
# audio_output, take mpd::output and get name
class mpd::server (
  String $music_directory,
  String $mpd_home                                            = '/var/lib/mpd',
  Boolean $follow_outside_symlinks                            = true,
  Boolean $follow_inside_symlinks                             = true,
  String $db_file                                             = "${mpd_home}/database",
  String $sticker_file                                        = "${mpd_home}/sticker.sql",
  String $log_file                                            = "${mpd_home}/log",
  String $pid_file                                            = "${mpd_home}/pid",
  String $playlist_directory                                  = "${music_directory}/playlists",
  String $state_file                                          = "${mpd_home}/state",
  String $user                                                = 'mpd',
  String $bind_to_address                                     = 'any',
  Integer $port                                               = 6600,
  Enum['default','secure','verbose'] $log_level               = 'default',
  Boolean $zeroconf_enabled                                   = true,
  Optional[String] $zeroconf_name                             = undef,
  Optional[Array[Password]] $password                         = undef,
  Optional[Array[Permission]] $default_permissions            = undef,
  Optional[String] $audio_output_format                       = undef,
  Optional[Variant[Integer,String]] $samplerate_converter     = undef,
  Optional[Enum['off', 'album', 'track', 'auto']] $replaygain = undef,
  Optional[Integer[-15,15]] $replaygain_preamp                = undef,
  Optional[Boolean] $volume_normalization                     = undef,
  Optional[Integer] $audio_buffer_size                        = undef,
  Optional[Integer[0,100]] $buffer_before_play                = undef,
  Optional[Integer] $connection_timeout                       = undef,
  Optional[Integer] $max_connections                          = undef,
  Optional[Integer] $max_playlist_length                      = undef,
  Optional[Integer] $max_output_buffer_size                   = undef,
  Optional[String] $filesystem_charset                        = undef,
  Optional[String] $id3v1_encoding                            = undef,
  Optional[Boolean] $gapless_mp3_playback                     = undef,
  Boolean $save_absolute_paths_in_playlists                   = true,
  Optional[Metadata] $metadata_to_use                         = undef,
  Boolean $auto_update                                        = true,
  Optional[Integer] $auto_update_depth                        = undef,
) {

  concat { '/etc/mpd.conf':
    ensure => present,
  }

  $hash = {
    music_directory => $music_directory,

    follow_outside_symlinks          => $follow_outside_symlinks,
    follow_inside_symlinks           => $follow_inside_symlinks,
    db_file                          => $db_file,
    sticker_file                     => $sticker_file,
    log_file                         => $log_file,
    pid_file                         => $pid_file,
    playlist_directory               => $playlist_directory,
    state_file                       => $state_file,
    user                             => $user,
    bind_to_address                  => $bind_to_address,
    port                             => $port,
    log_level                        => $log_level,
    zeroconf_enabled                 => $zeroconf_enabled,
    save_absolute_paths_in_playlists => $save_absolute_paths_in_playlists,
    auto_update                      => $auto_update,


    zeroconf_name        => $zeroconf_name,
    password             => $password,
    default_permissions  => $default_permissions,
    audio_output_format  => $audio_output_format,
    samplerate_converter => $samplerate_converter,
    replaygain           => $replaygain,
    replaygain_preamp    => $replaygain_preamp,
    volume_normalization => $volume_normalization,
    audio_buffer_size    => $audio_buffer_size,
    buffer_before_play   => $buffer_before_play,
    filesystem_charset   => $filesystem_charset,
    id3v1_encoding       => $id3v1_encoding,
    gapless_mp3_playback => $gapless_mp3_playback,
    metadata_to_use      => $metadata_to_use,
    auto_update_depth    => $auto_update_depth,
  }


  concat::fragment { 'mpd_base_config':
    target  => '/etc/mpd.conf',
    content => $hash
          .filter |$k, $v| { $v != undef }
          .map |$k, $v| {
            $val = $v ? {
              Boolean => mpd::bool_to_yesno($v),
              default => $v,
            }
            "${k} \"${val}\"\n"
          }
          .join()
  }

  service { 'mpd':
    ensure => 'running',
    enable => true,
  }

  case $facts['os']['family'] {
    'RedHat': {
      yumrepo { 'rpm-fusion':
        ensure   => present,
        baseurl  => 'https://download1.rpmfusion.org/free/el/updates/7/x86_64',
        enabled  => true,
        gpgcheck => true,
        gpgkey   => 'https://rpmfusion.org/keys?action=AttachFile&do=get&target=RPM-GPG-KEY-rpmfusion-free-el-7',
      }

      package { ['mpd']:
        ensure  => installed,
        require => Yumrepo['rpm-fusion'],
      }
    }
    default: {
      fail('Currently only works on CentOS')
    }
  }
}
