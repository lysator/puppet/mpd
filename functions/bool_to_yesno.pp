function mpd::bool_to_yesno (
  Boolean $bool,
) >> String {
  if $bool { 'yes' } else { 'no' }
}
